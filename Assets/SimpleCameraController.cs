﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace UnityTemplateProjects {
    public class SimpleCameraController : MonoBehaviour {
        public float speedPitch = 2.0f;
        public float speedYaw = 2.0f;
        public float speedWalk = 2;
        public Rigidbody cc;
        private void Awake() {
            cc = GetComponent<Rigidbody>();
            mCamera = transform.GetComponentInChildren<Camera>();
        }

        private Camera mCamera;

        private Vector3 GetInputTranslationDirection() {
            Vector3 direction = Vector3.zero;
            if(Input.GetKey(KeyCode.W)) {
                direction += Vector3.forward;
            }
            if(Input.GetKey(KeyCode.S)) {
                direction += Vector3.back;
            }
            if(Input.GetKey(KeyCode.A)) {
                direction += Vector3.left;
            }
            if(Input.GetKey(KeyCode.D)) {
                direction += Vector3.right;
            }
            if(Input.GetKey(KeyCode.Q)) {
                direction += Vector3.down;
            }
            if(Input.GetKey(KeyCode.E)) {
                direction += Vector3.up;
            }

            return direction;
        }

        private int mouseButton = 0;

        public float minTilt = -45.0f;
        public float maxTilt = 45.0f;


        private void FixedUpdate() {
            if(Input.GetMouseButton(0) && !Input.GetMouseButtonDown(mouseButton)) {
                float x = Input.GetAxis("Mouse X") * speedPitch*Time.deltaTime*100f;
                float y = -Input.GetAxis("Mouse Y") * speedYaw*Time.deltaTime*100f;
                mCamera.transform.localEulerAngles = new Vector3(Mathf.Clamp(Mathf.Repeat(Mathf.Repeat(mCamera.transform.localEulerAngles.x,360f)+180f,360f) + y,minTilt+180,maxTilt+180)-180, 0, 0);
                cc.rotation = Quaternion.Euler(cc.rotation.eulerAngles + new Vector3(0, x, 0));
            }

            Vector3 movement = GetInputTranslationDirection();
            float boost = 1;
            if(Input.GetKey(KeyCode.LeftShift)||Input.GetKey(KeyCode.RightShift)) {
                boost = 2;
            }
            cc.velocity =  transform.TransformVector( movement * speedWalk*boost) ;
            
            float yVel = cc.velocity.y;
            Vector3 newVel = cc.velocity;
            newVel.y += yVel;
            cc.velocity = newVel;
            
            //Jump
            if (Input.GetKeyDown(KeyCode.Space) && transform.localPosition.y <= 1.0f )
            {
                cc.AddForce(Vector3.up * 20, ForceMode.Impulse);
            }
        }
    }
}
